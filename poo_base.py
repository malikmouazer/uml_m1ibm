class MaClasse:
    # Pour l'instant, la classe est déclarée vide,
    # d'où l'utilisation du mot-clé 'pass'.
    # C'est une classe qui ne fait rien

    # On peut defnir :
        # des fonctions (qui deviendront des méthodes de la classe) ;
        #des variables (qui deviendront des attributs de la classe) ;
        #des classes imbriquées, internes à la classe principale.
    pass


#il est possible, voire conseillé,
#de répartir les classes d’une application dans plusieurs fichiers
#(qu’on appelle « modules » en Python)


# Classe contenante, déclarée normalement.
class Humain :

    # Classes contenues, déclarées normalement aussi,
    # mais dans le corps de la classe contenante.

    class Femme :
        pass

    class Homme:
        pass
#La seule implication de l’imbrication est qu’il faut désormais passer par la classe Humain pour utiliser les classes
# Femme et Homme en utilisant l’opérateur d’accès « point » : Humain.Femme et Humain.Homme.
#Exactement comme on le ferait pour un membre classique.

# L’imbrication n’impacte en rien le comportement du contenant ou du contenu.

instance = MaClasse()
# L’omission des parenthèses signifie que l’on désigne la classe elle-même.
c = MaClasse
print("c: ", c)
print(instance) # Un costructeur par défaut est appellé : __main__

print(__name__) # C'est le nom du module dans lequel a été declarée MaClasse ; c'est l'actuel main



#b. Méthode
#Une méthode est une fonction définie dans une classe ayant comme premier argument une instance de cette classe.

class Cercle:

    def __init__(self, rayon):
        self.rayon = rayon

    def diametre(self):
        return self.rayon * 2
    def perimetre(self):
        # Définition du corps de la méthode,
        # en l'occurrence avec une valeur de retour.
        return 2 * 3.14 * self.rayon

c = Cercle(4)

# Appel de la méthode perimetre() de l'instance c.
print(c.perimetre())

# Constructeur
#Pour des raisons de facilité d’écriture et de flexibilité,
#il est possible de personnaliser le processus d’instanciation d’une classe en implémentant la méthode __init__ de cette classe :
