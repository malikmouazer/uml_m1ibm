import datetime

class Pa(object):

    def __init__(self, dci):
        self.dci = dci
        #self.dose = dose
        self.contre_indics = []
        #data = {"maladie": :Maladie, "risque": "balbal"}
        self.interactions = []
        #data = {"medoc": :Pa, "risque": "balbal"}
        self.indications = []
        #data = {"medoc": :Pa, "risque": "balbal"}


class Medicament(object):

    def __init__(self, nom):
        self.nom = nom
        self.pas = []

class Maladie(object):
    """docstring for Maladie."""

    def __init__(self, nom, code_cim):
        self.nom= nom
        self.code_cim = code_cim


class Prescription(object):
    """docstring for Prescription."""
    # Ne s'utilise pas pour l'instant

    def __init__(self, date = datetime.datetime.now()):
        self.date = date
        self.medocs = []

class DossierPatient(object):
    """docstring for DossierPatient."""

    def __init__(self, nom):

        self.nom = nom
        self.maladies = []
        self.medocs = []

    def prescrire(self, medoc):
        self.medocs.append(medoc)

    def diagnostiquer(self, maladie):
        self.maladie.append(maladies)

    def get_interactions(self):
        medocs = self.medocs
        tot_pas = []
        for med in medocs :
            for p in med.pas:
                tot_pas.append({"med" : med, "pa" : p})
        ls_interactions = []
        #print(tot_pas)
        if(len(tot_pas)>1):

            for p1 in tot_pas:
                for p2 in tot_pas:
                    for inter in p1["pa"].interactions:
                        if(inter["medoc"] == p2["pa"]):
                            ls_interactions.append({"m1" : p1["med"].nom,
                                                    "m2" : p2["med"].nom,
                                                    "texte" : inter["risque"]})
            if(len(ls_interactions)>0):
                for interaction in ls_interactions:
                    print("  🚨", interaction["m1"], " ⚡⚡ " ,interaction["m2"], " : ", interaction["texte"])
            else:
                print("------ ✖ Aucune interaction médicamenteuse detectée ✖ ------")
        else:
            print("------ 🧐 Fonctionnalité non accessible  ✖ Le patient est sous monothérapie ------")
        #return ls_interactions
    def get_contre_indications(self, medocs, maladies):
        print("Liste des contre indications")
    def deprescrire(self, medoc):
        medocs = self.medocs
        medocs.remove(medoc)
    def remove_diag(self, mal):
        maladies.remove(mal)
