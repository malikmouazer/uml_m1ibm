from dp import *
from dossiers import *
from maladies import *
from drugs import *



ls_medocs_dispo = [glucophage, janumet, cardensiel, lasilix, deroxat]
ls_maladies = [asthme, hta, diabete, depression, insuf_card]
mes_patients = [dp1, dp2]

def display_menu_principal(dp):
    indice_pat = int(dp) - 1
    patient = mes_patients[indice_pat]
    print("🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹🔹")
    print("-----------------------🧬 Antecedents et diagnostics-------------------")
    print(patient.nom, " souffre actuellement de : ")
    for mal in patient.maladies:
        print("    ", mal.nom)
    print("------------------------------💉 Traitement----------------------------")
    if(len(patient.medocs) < 1):
        print("------ ✖ Aucun traitement médicamenteux n'est disponible ✖ ------")
    else :
        print("Son traitement médicamenteux est le suivant : ")
        for med in patient.medocs:
            print("    ", med.nom)
    print("##################--------Menu Principal-------######################")
    print("1]  💊 Prescrire un médicament    2] ⚕ deprescrire un médicament ")
    print("3]  🫁  Poser un diagnostic       4] ⚠ Verifier la sécurité des prescriptions ")
    print("--------------------------------------------------------------------")
    print("🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻")

    action = input("Que souhaitez-vous faire ? : ")

    return action



def display_right_menu(choix, d):
    #match agrument:
    if choix == 1:
        display_menu_prescrire(d)
    elif choix == 2:
        display_menu_deprescrire(d)
    elif choix == 3:
        display_menu_diagnostiquer(d)
    elif choix == 4:
        display_menu_securiser(d)




def display_menu_prescrire(dp):
    print("🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩")
    print("-----------------------Prescription de médicaments-------------------")
    print("-----------------------Médicaments disponibles :")
    for i in range(len(ls_medocs_dispo)) :
        num_med = i + 1
        print(num_med, ls_medocs_dispo[i].nom)
    print("🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩")

    choix = input("Quel médicament prescrire ? : ")
    index_medoc = int(choix) - 1
    mes_patients[dp-1].prescrire(ls_medocs_dispo[index_medoc])
    display_right_menu(int(display_menu_principal(dp)), int(dp))

def display_menu_deprescrire(dp):
    print("🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩")
    print("------------ Supprimer un médicament du dossier patient --------------")


    my_medocs = mes_patients[dp-1].medocs
    if(len(my_medocs)>0):
        for i in range(len(my_medocs)) :
            num_med = i + 1
            print(num_med, my_medocs[i].nom)
        print("🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩")

        choix = input("Quel médicament Dé-prescrire ? : ")
        index_medoc = int(choix) - 1
        mes_patients[dp-1].deprescrire(my_medocs[index_medoc])
        display_right_menu(int(display_menu_principal(dp)), int(dp))

    else:
        print("🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩")
        print("🛑 Le patient ne consomme aucun médicament, vous ne pouvez pas utiliser cette fonctionnalité !")
        print("👉 Retour au menu principal")
        print("🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩")
        display_right_menu(int(display_menu_principal(dp)), int(dp))


def display_menu_diagnostiquer(dp):
    print("🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩")
    print("Posez votre Diagnostic")
    print("🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩")


def display_menu_securiser(dp):
    print("🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩")
    print("-----------------------Liste des Intéractions médicamenteuses-------------------")

    mes_patients[dp-1].get_interactions()
    
    print("----------------------------------##############--------------------------------")
    print("-----------------------Liste des Contre indications-------------------")
    print("🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩")

    display_right_menu(int(display_menu_principal(dp)), int(dp))
