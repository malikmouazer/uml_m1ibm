from dp import *
from drugs import *
from maladies import *

#daenerys targaryen

dp1 =  DossierPatient("daenerys targaryen")

dp1.maladies = [depression]

# Joseph Wilford
dp2 =  DossierPatient("Joseph Wilford")
dp2.maladies = [diabete, hta, insuf_card, asthme]
dp2.medocs = [janumet, cardensiel, lasilix, deroxat]
#dp2.prescrire(glucophage)
#print(dp2.medocs)

#maladies = dp.maladies

def check_inters(dp):
    medocs = dp.medocs
    tot_pas = []
    for med in medocs :
        for p in med.pas:
            tot_pas.append({"med" : med, "pa" : p})
    ls_interactions = []
    #print(tot_pas)
    if(len(tot_pas)>1):

        for p1 in tot_pas:
            for p2 in tot_pas:
                for inter in p1["pa"].interactions:
                    if(inter["medoc"] == p2["pa"]):
                        ls_interactions.append({"m1" : p1["med"].nom,
                                                "m2" : p2["med"].nom,
                                                "texte" : inter["risque"]})
    return ls_interactions

#print(check_inters(dp2))
#dp2.get_interactions()
