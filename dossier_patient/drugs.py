from dp import *

##########################################--------Les principes actifs--------##################################################


metformine = Pa("metformine")
metformine.contre_indics = ["I50"]

paroxetine = Pa("paroxetine")




furosemide = Pa("furosemide")
furosemide.interactions = [{"medoc" : metformine, "risque" : """ Using furosemide together with metFORMIN may increase the effects of metFORMIN, which may lead to a life-threatening condition called lactic acidosis"""},
                            {"medoc" : paroxetine, "risque" : """ paroxetine may occasionally cause blood sodium levels to get too low, a condition known as hyponatremia, and using it with furosemide can increase that risk"""}]

sitagliptine = Pa("Sitagliptine")
sitagliptine.interactions = [{"medoc" : furosemide, "risque" :  """Furosemide may interfere with blood glucose control and reduce the effectiveness of SITagliptin """}]
bisoprolol = Pa("Bisoprolol")
bisoprolol.interactions = [{"medoc" : furosemide, "risque" : """ Using furosemide and bisoprolol together may lower your blood pressure and slow your heart rate"""}]
bisoprolol.contre_indics = ["J45"]

##########################################-------------Médicaments----------####################################################


lasilix = Medicament("Lasilix")
lasilix.pas = [furosemide]


glucophage = Medicament("Glucophage")
glucophage.pas = [metformine]


janumet = Medicament("Janumet")
janumet.pas = [metformine, sitagliptine]

cardensiel = Medicament("Cardensiel")
cardensiel.pas = [bisoprolol]

deroxat = Medicament("Deroxat")
deroxat.pas = [paroxetine]

#print(janumet.pas)
