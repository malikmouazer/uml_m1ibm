# La classe de base.
class Forme:

    # La méthode polymorphe, qui sera spécialisée
    # par chaque classe dérivée.
    def perimetre(self):
        # Message d'erreur.
        raise NotImplementedError("Impossible de calculer le périmètre d'une forme générique")


# Une classe dérivée.
class Carre(Forme):

    def __init__(self, cote):
        super().__init__()
        self.cote = cote

    # Surchage de la méthode de base.
    def perimetre(self):
        return 4 * self.cote

# Une autre classe dérivée.
class Cercle(Forme):

    def __init__(self, rayon):
        super().__init__()
        self.rayon = rayon

    # Une nouvelle surcharge de la méthode de base.
    def perimetre(self):
        return 2 * 3.14 * self.rayon

# Création d'une liste de formes concrètes.
formes = [Cercle(3), Carre(2), Carre(5)]


# Parcours de la liste de formes, sans se soucier
# de quelles formes concrètes il s'agit.
for forme in formes:
    print(forme.perimetre())


# Classe qui n'hérite pas de la classe de base Forme.
class Choucroute:

    # Définition d'une méthode ayant le même prototype
    # que celle surchargée dans Forme.
    def perimetre(self):
        return "Aucun rapport"

    # Déclaration d'une liste de formes, contenant aussi
    # une instance de cette nouvelle classe « incongrue ».
formes = [Cercle(3), Carre(2), Carre(5), Choucroute()]

# Parcours de la liste comme si de rien n'était...
for forme in formes:
    print(forme.perimetre())
