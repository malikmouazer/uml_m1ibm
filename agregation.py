# L’agrégation est une relation contenu-contenant dite « faible »,
#  dans le sens où le contenu survit à la destruction de son contenant


class Voiture:

    def __init__(self):
        self.roues = []

    def ajouterRoue(self, roue):
        # Si la liste contient moins de cinq éléments...
        if (len(self.roues) < 5):
            # ... on y ajoute la roue donnée en paramètre.
            self.roues.append(roue)
            # On en profite pour mettre à jour le lien
            # entre la roue et la voiture.
            roue.voiture = self
        else:
            print("La voiture possède déjà 5 roues.")

class Roue:

    def __init__(self):
        # Déclaration d'un attribut contenant une référence
        # vers la voiture à laquelle est rattachée la roue.
        self.voiture = None


# Création d'une liste avec quatre instances de Roue.
roues = [Roue() for i in range(6)]
# Création d'une instance de Voiture.

print(len(roues))
voiture = Voiture()

for roue in roues:
    # # Assignation de la liste de roues à la Voiture.
    voiture.ajouterRoue(roue)

print("Instance de voiture : {}".format(voiture))



print("Les 5 roues de la voiture {} sont {}".format(voiture, voiture.roues))



print("La voiture de la roue {} est {}".format(roues[0], roues[0].voiture))

del(voiture) # Meme si je supprime la voitire j'ai toujours mes roues
print("J'ai {} roues qui sont {}".format(len(roues), roues))
