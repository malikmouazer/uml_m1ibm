# Une première classe de base.
class Druide:

     def __init__(self):
        super().__init__()
        self.magie = 4
        self.pv_base = 10
     def pv(self):
        return self.pv_base * 3
# Une deuxième classe de base.
class Voleur:

    def __init__(self):
        super().__init__()
        self.dexterite = 7
        self.pv_base = 10
    def pv(self):
        return self.pv_base * 2


# Une classe dérivée double.
class Heros(Druide, Voleur):
    def __init__(self):
        super().__init__()
    def pv(self):
        return (Druide.pv(self) + Voleur.pv(self)) / 2


h = Heros()
print(isinstance(h, Voleur))
print(isinstance(h, Druide))


print(h.magie, "; ", h.dexterite, h.pv())

# l'Heritage multiple peut etre allechant mais il faut faire attention aux nom des methodes et attributs communs aux classses soeur surtout, ça peut generer un comportement inattendu.

class Point2D:

    def __init__(self, x, y):
         self.x = x
         self.y = y

    def translation(self, a, b):
         self.x += a
         self.y += b

    # Afin d'avoir un affichage "propre" d'un point.
    def __str__(self):
         return "X: {}; Y: {}".format(self.x, self.y)

xy = Point2D(10, 10)

print("xy : {}".format(xy))

xy.translation(1, 3)

print("xy : {}".format(xy))
# Un point 3D est un point 2D avec une coordonnée supplémentaire :
# l'axe z. On hérite donc de Point2D pour le spécialiser.
class Point3D(Point2D):

    def __init__(self, x, y, z):
        # Initialisation des coordonnées x et y du point via
        # le constructeur de Point2D : pas d'accès direct ici.
        super().__init__(x, y)
        # Initialisation de la coordonnée z.
        self.z = z

    # Une translation en 3D suit le même principe
    # qu'une translation en 2D.
    def translation(self, x, y, z):
        # On réutilise donc la méthode de Point2D :
        # pas d'accès direct ici non plus.
        super().translation(x, y)
        # Et on y ajoute le traitement spécifique.
        # aux points 3D.
        self.z += z

    def __str__(self):
        _2d = super().__str__()
        return "{}; Z = {}".format(_2d, self.z)


xyz = Point3D(10, 10, 10)

print("xyz : {}".format(xyz))

xyz.translation(1, 3, 2)

print("xyz : {}".format(xyz))
