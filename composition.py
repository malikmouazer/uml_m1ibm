#Une composition n’est autre qu’une agrégation où le contenu ne survit pas à son contenant

#Afin de lier la destruction d’un attribut à celui de l’instance qui le contient,
#il suffit d’instancier cet attribut dans le constructeur du contenant.
#Le destructeur d’une classe appelle automatiquement les destructeurs de ses attributs.


class Maison:

    def __del__(self):
        print("Destruction de la maison")

    def __init__(self):
        # Déclaration d'une liste d'instances de Piece
        # avec des noms différents.
        self.pieces = [Piece(name) for name in ["Cuisine", "Chambre", "Salon"]]

class Piece:

    def __del__(self):
        print("Destruction de {}".format(self.name))

    def __init__(self, name):
        self.name = name

maison = Maison()
del maison


class Voiture:
    def __del__(self):
        print("Destruction de la voiture")

    def __init__(self):
        self.roues = [Roue(name) for name in ["RS", "AVG", "AVD", "ARG", "ARD", "RS2"]]


class Roue:
    def __del__(self):
        print("Destruction de {}".format(self.name))

    def __init__(self, name):
        # Déclaration d'un attribut contenant une référence
        # vers la voiture à laquelle est rattachée la roue.
        self.name = name

#nom_roues =

cocci = Voiture()
for roue in cocci.roues :
    print(roue.name)
del cocci

print(cocci)
