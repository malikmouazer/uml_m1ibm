#Énoncé : modélisez ce qui suit. Une entreprise possède plusieurs bâtiments et emploie plusieurs employés. Un bâtiment se situe forcément dans une ville, et une ville se compose de plusieurs bâtiments. Entreprise, employé, ville et bâtiment possèdent chacun un nom. Parmi ces villes, on trouve New York dans laquelle se situent les bâtiments A et B, et Los Angeles dans laquelle se situe le bâtiment C. Ces trois bâtiments appartiennent à l’entreprise YooHoo ! qui emploie messieurs Martin, Salim et madame Xing.

#Une fois ces entités en place, imaginez que votre programme est un film catastrophe américain dans lequel New York est détruite. Implémentez cet événement de sorte que les conséquences de ce cataclysme soient bien prises en compte par toutes les entités en jeu.

#Corrigé : ce code n’est qu’une implémentation possible du problème. À vous de vérifier dans votre code que les bâtiments et villes sont bien détruits. Si c’est le cas, alors votre solution est correcte.

class Ville:
    def __init__(self, nom):
        self.nom = nom
        self.batiments = []

    def __del__(self):
        print("Destruction de {}".format(self.nom))

class Batiment:
    def __init__(self, nom):
        self.nom = nom

    def __del__(self):
        print("Destruction de {}".format(self.nom))

class Entreprise:
    def __init__(self, nom):
        self.nom = nom
        self.batiments = []
        self.employes = []

    def __del__(self):
        print("Dépôt de bilan de {}".format(self.nom))

    def recrute(self, employe):
        self.employes.append(employe)

    def nombreDeBatiments(self):
        print("{} possède {} bâtiment(s)".format(self.nom,
len(self.batiments)))

class Employe:
    def __init__(self, nom):
        self.nom = nom

    def __del__(self):
        print("Licenciement ou démission de {}".format(self.nom))

# Instanciation de la ville de New York.
ny = Ville("New York")
# Assignation d'une liste de nouvelles instances de Batiment
# à la ville.
ny.batiments = [Batiment("A"), Batiment("B")]
# Idem pour Los Angeles.
lax = Ville("Los Angeles")
lax.batiments = [Batiment("C")]

# Instanciation de l'entreprise.
entreprise = Entreprise("YooHoo!")
# Assignation d'une liste d'instances de Batiment déjà existantes
# à l'entreprise.
entreprise.batiments = [ny.batiments[0], ny.batiments[1], lax.batiments[0]]

# Instanciation de nouveaux employés.
martin = Employe("Martin")
salim = Employe("Salim")
xing = Employe("Xing")
[entreprise.recrute(employe) for employe in [martin, salim, xing]]

# Si l'on détruit l'instance de Ville, les bâtiments A et B
# ne sont pas détruits car ils sont référencés dans la liste
# entreprise.batiments. Il faut donc les supprimer de cette liste
# pour que les bâtiments soient bien détruits avec la ville.
# Cette fonction effectue cette tâche.
def detruireBatimentsEntreprise(ville):
    # Pour tous les bâtiments de la ville détruite...
    for batiment in ville.batiments:
        # ...si le bâtiment fait partie des bâtiments
        # de l'entreprise...
        if batiment in entreprise.batiments:
            # ...on retire ce bâtiment du patrimoine de l'entreprise.
            entreprise.batiments.remove(batiment)

entreprise.nombreDeBatiments()
# YooHoo! possède trois bâtiment(s)

detruireBatimentsEntreprise(ny)
entreprise.nombreDeBatiments()
#YooHoo! possède 1 bâtiment(s)

# Suppression de la seule référence à l'instance de New York
del ny


# Suppression de la seule référence à l'instance de l'entreprise.
del entreprise
# Dépôt de bilan de YooHoo!

# Fin du programme. Le ramasse-miettes supprime les dernières
# instances dans l'ordre qui lui convient.
# Destruction de Los Angeles
