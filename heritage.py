# Définition de la classe de base.
class Forme:
    # Constructeur de la classe de base.
    def __init__(self):
        print("Initier une Forme", self)
        # Initialisation des attributs d'instance.
        self.x = 0
        self.y = 0

# Définition de la classe dérivée.
class Cercle(Forme):
    # Constructeur de la classe dérivée, qui n'appelle pas
    # le constructeur de la classe de base.
    def __init__(self):
        # Appel explicite au constructeur de la classe de base. ça doit se faire d'une maniere explicite sinon ça n'herite pas
        #Forme.__init__(self) # comme ça
        # Ou d'une ma,iere plus élégante
        super().__init__()
        print("Initier un Cercle", self)

c = Cercle()
print(c.x, c.y)

print(type(c))
print(isinstance(c, Forme))
print(isinstance(c, Cercle))

print(issubclass(Cercle, Forme))  
