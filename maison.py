class Mur:
   def __init__(self, orientation):
       self.orientation = orientation
       # Un mur n'a aucune fenêtre par défaut
       self.fenetres = []

class Fenetre:
   def __init__(self, mur, surface, protection):
       self.mur = mur
       self.surface = surface
       # On "attache" la fenêtre au mur donné en paramètre
       self.mur.fenetres.append(self)
       if protection is None:
           # Interruption du programme
           # car cette situation est jugée incohérente
           raise Exception("Protection obligatoire")
       self.protection = protection



class Maison:
   def __init__(self, murs):
       self.murs = murs

   def surface_vitree(self):
       surface = 0
       for mur in self.murs:
           for fenetre in mur.fenetres:
               surface += fenetre.surface
       return surface



#
class MurRideau(Mur, Fenetre):
   def __init__(self, orientation, surface, protection):
       # Appel au constructeur de Mur afin d'avoir une instance
       # sur laquelle "attacher" la composante "fenêtre"
       Mur.__init__(self, orientation)
       # Appel au constructeur de Fenetre, avec 'self' comme premier
       # paramètre 'mur' (car un MurRideau est un Mur).
       Fenetre.__init__(self, self, surface, protection)

# Instanciation des murs
mur_nord = Mur("NORD")
mur_ouest = Mur("OUEST")
mur_sud = MurRideau("SUD", 5, "ProMAx")
mur_est = Mur("EST")
# Instanciation des fenêtres
fenetre_nord = Fenetre(mur_nord, 0.5,"double vitrage")
fenetre_nord_2 = Fenetre(mur_nord, 0.5,"double vitrage")
fenetre_ouest = Fenetre(mur_ouest, 1, "Volet")
#fenetre_sud = Fenetre(mur_sud, 2, "double vitrage")
fenetre_est = Fenetre(mur_est, 1, "isolation thermique")
# Instanciation de la maison avec les 4 murs
maison = Maison([mur_nord, mur_ouest, mur_sud, mur_est])

print(mur_nord.fenetres)
print(maison.surface_vitree())
